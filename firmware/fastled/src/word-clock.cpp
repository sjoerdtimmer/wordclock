#define FASTLED_ESP8266_RAW_PIN_ORDER
#define FASTLED_ALLOW_INTERRUPTS 0
#include <Arduino.h>
#include "FastLED.h"
#include <FS.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <ArduinoJson.h>
#include <TimeLib.h>
#include <ESP8266HTTPClient.h>
#include <vector>
#include "SunSet.h"
#include "gamma.h"

#define DUTCH
//#define ENGLISH

//#define R_VALUE         50
//#define G_VALUE         255
//#define B_VALUE         0

#define MIN_BRIGHTNESS  10
#define MAX_BRIGHTNESS  150

#define LATITUDE 51.8126
#define LONGITUDE 5.8372
SunSet sun;


#define SYNC_INTERVAL   1200
//#define SYNC_INTERVAL   5

#define NUM_LEDS        95
#define DATA_PIN        D2 // D2 Pin on Wemos mini

#define LDR_PIN         A0

const int   timeZone        = 1;     // Central European Time
bool        autoDST         = true;

IPAddress   timeServerIP; // time.nist.gov NTP server address
const char* ntpServerName   = "nl.pool.ntp.org";
//const char* ntpServerName   = "192.168.2.22";
const int   NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
byte        packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
WiFiUDP     Udp;
unsigned int localPort = 8888;

time_t getNtpTime();
void sendNTPpacket(IPAddress &address);
void rainbow();
bool isDST(int d, int m, int y);
bool isDSTSwitchDay(int d, int m, int y);
    
unsigned long   lastdisplayupdate   = 0;

CRGB leds[NUM_LEDS];

uint8_t targetlevels[3][NUM_LEDS];
uint8_t currentlevels[3][NUM_LEDS];

void setup() {
    Serial.begin(115200);

    WiFiManager wifiManager;

    ArduinoOTA.onStart([]() {
            Serial.println("Start");
            });
    ArduinoOTA.onEnd([]() {
            Serial.println("\nEnd");
            });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
            Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
            });
    ArduinoOTA.onError([](ota_error_t error) {
            Serial.printf("Error[%u]: ", error);
            if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
            else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
            else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
            else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
            else if (error == OTA_END_ERROR) Serial.println("End Failed");
            });
    ArduinoOTA.begin();

    sun.setPosition(LATITUDE, LONGITUDE, 0);

    String ssid = "WordClock-" + String(ESP.getChipId());
    wifiManager.autoConnect(ssid.c_str());

    Serial.println("Connected!");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.println("Starting UDP");
    Udp.begin(localPort);
    LEDS.addLeds<NEOPIXEL,DATA_PIN>(leds,NUM_LEDS);
    LEDS.setBrightness(87);
    rainbow();
    LEDS.setBrightness(MIN_BRIGHTNESS);
    for(int i=0;i<NUM_LEDS;i++) {
        for (uint8_t c=0; c<3; c++) {
            targetlevels[c][i] = 0;
            currentlevels[c][i] = 0;
        }
        leds[i] = CRGB::Black;
    }

    FastLED.show();

    setSyncProvider(getNtpTime);
    setSyncInterval(10);

    pinMode(LDR_PIN, INPUT);
}

void rainbow() {
    uint8_t gHue = 0;
    while (gHue < 255) {
        EVERY_N_MILLISECONDS(20) {gHue++;}
        fill_rainbow(leds, NUM_LEDS, gHue, 1);
        FastLED.delay(1000/30); // 30FPS
    }
}

/*

   HET IS X UUR
   HET IS VIJF OVER X
   HET IS TIEN OVER X
   HET IS KWART OVER X
   HET IS TIEN VOOR HALF (X+1)
   HET IS VIJF VOOR HALF (X+1)
   HET IS HALF (X+1)
   HET IS VIJF OVER HALF (X+1)
   HET IS TIEN OVER HALF (X+1)
   HET IS KWART VOOR (X+1)
   HET IS TIEN VOOR (X+1)
   HET IS VIJF VOOR (X+1)
   HET IS (X+1) UUR
   ...
*/

#ifdef DUTCH
#define HET    0
#define IS    20
#define VIJF  13
#define TIEN  14
#define KWART 15
#define VOOR  16
#define OVER  17
#define HALF  18
#define UUR   19

std::vector<std::vector<int>> ledsbyword = {
    {84,85,86},         // HET
    {4,5,6},            // een
    {47,48,49,50},      // twee
    {43,42,41,40},      // drie
    {23,22,21,20},      // vier
    {7,8,9,10},         // vijf
    {51,52,53},         // zes
    {25,26,27,28,29},   // zeven
    {44,45,46,47},      // acht
    {29,30,31,32,33},   // negen
    {37,36,35,34},      // tien
    {40,39,38},         // elf
    {19,18,17,16,15,14},// twaalf
    {82,81,80,79},      // VIJF
    {78,77,76,75},      // TIEN
    {64,65,66,67,68},   // KWART
    {63,62,61,60},      // VOOR
    {70,71,72,73},      // OVER
    {58,57,56,55},      // HALF
    {11,12,13},         // UUR
    {89, 90}            // IS
};

std::vector<std::vector<uint8_t>> wordcolors = {
    {  0, 255,   0}, // HET
    {255, 255,   0}, // een
    {255, 135,   0}, // twee
    {255, 255,   0}, // drie
    {255, 135,   0}, // vier
    {255, 255,   0}, // vijf
    {255, 135,   0}, // zes
    {255, 255,   0}, // zeven
    {255, 135,   0}, // acht
    {255, 255,   0}, // negen
    {255, 135,   0}, // tien
    {255, 255,   0}, // elf
    {255, 135,   0}, // twaalf
    {255,   0,   0}, // VIJF
    {255,   0, 255}, // TIEN
    {255,   0,   0}, // KWART
    {  0, 255,   0}, // VOOR
    {255, 255,   0}, // OVER
    {  0, 135, 255}, // HALF
    {  0, 135, 255}, // UUR 
    {  0, 255, 255}  // IS
};
#endif

#ifdef ENGLISH
#define IT 0
#define IS 13
#define FIVE 14
#define TEN 15
#define QUARTER 16
#define TWENTY 17
#define TWENTYFIVE 18
#define HALF 19
#define PAST 20
#define TO 21
#define OCLOCK 22
#define A 23

std::vector<std::vector<int>> ledsbyword = {
    {84,85},                        // IT
    {46,47,48},                     // one
    {44,45,46},                     // two
    {38,37,36,35,34},               // three
    {57,56,55,54},                  // four
    {23,22,21,20},                  // five
    {4,5,6},                        // six
    {43,42,41,40,39},               // seven
    {29,30,31,32,33},               // eight
    {17,16,15,14},                  // nine
    {19,18,17},                     // ten
    {48,49,50,51,52,53},            // eleven
    {24,25,26,27,28,29},            // twelve
    {87,88},                        // IS
    {70,71,72,73},                  // FIVE
    {76,75,74},                     // TEN
    {83,82,81,80,79,78,77},         // QUARTER
    {64,65,66,67,68,69},            // TWENTY
    {64,65,66,67,68,69,70,71,72,73},// TWENTYFIVE
    {90,91,92,93},                  // HALF
    {63,62,61,60},                  // PAST
    {60,59},                        // TO
    {8,9,10,11,12,13},              // OCLOCK
    {91}                            // A           
};

std::vector<std::vector<uint8_t>> wordcolors = {
    {  0, 255,   0}, // IT
    {255, 255,   0}, // one
    {255, 135,   0}, // two
    {255, 255,   0}, // three
    {255, 135,   0}, // four
    {255, 255,   0}, // five
    {255, 135,   0}, // six
    {255, 255,   0}, // seven
    {255, 135,   0}, // eight
    {255, 255,   0}, // nine
    {255, 135,   0}, // ten
    {255, 255,   0}, // eleven
    {255, 135,   0}, // twelve
    {  0, 255, 255}, // IS
    {255,   0,   0}, // FIVE
    {255,   0, 255}, // TEN
    {255,   0,   0}, // QUARTER
    {255,   0, 255}, // TWENTY
    {  0, 255, 255}, // TWENTYFIVE
    {255,   0,   0}, // HALF
    {  0, 255, 255}, // PAST
    {  0, 255,   0}, // TO
    {  0, 135, 255}, // OCLOCK
    {  0, 255,   0}  // A
};

#endif



int smoothed_brightness = 0;
int ldrBrightness() {
    int ldr_val = analogRead(LDR_PIN);
    int brightness = ldr_val/4;
    if (brightness > smoothed_brightness) smoothed_brightness++;
    else if (brightness < smoothed_brightness) smoothed_brightness--;
    smoothed_brightness = constrain(smoothed_brightness, MIN_BRIGHTNESS, MAX_BRIGHTNESS);
    return smoothed_brightness;
}

int sunsetBrightness() {
    if (isDST(year(), month(), day())) {
        sun.setTZOffset(2);
    }else {
        sun.setTZOffset(1);
    }
    sun.setCurrentDate(year(), month(), day());
    int sunrise = sun.calcSunrise();
    int sunset = sun.calcSunset();
    char msg[255];
    //sprintf(msg, "sunrise: %02d:%02d\nsunset: %02d:%02d", sunrise/60, sunrise%60, sunset/60, sunset%60);
    //Serial.println(msg);
    int brightness;
    if (hour() < 12) {
        brightness = map(60*hour()+minute(), sunrise-10, sunrise+10, MIN_BRIGHTNESS, MAX_BRIGHTNESS);
    } else {
        brightness = map(60*hour()+minute(), sunset-10, sunset+10, MAX_BRIGHTNESS, MIN_BRIGHTNESS);
    }
    return constrain(brightness, MIN_BRIGHTNESS, MAX_BRIGHTNESS);
}

// helper:
void show_word(uint8_t word) {
    for(int w : ledsbyword[word])   {
        targetlevels[0][w] = wordcolors[word][0];
        targetlevels[1][w] = wordcolors[word][1];
        targetlevels[2][w] = wordcolors[word][2];
    }
}

void loop() {
    // put your main code here, to run repeatedly:
    ArduinoOTA.handle();

    //Serial.println(analogRead(LDR_PIN));
    //  Serial.println("loop");

    // only update clock every 50ms
    if(millis()-lastdisplayupdate > 50) {
        lastdisplayupdate = millis();
    }else{
        return;
    }

    //int brightness = sunsetBrightness();
    //Serial.print("brightness: ");
    //Serial.println(brightness);
    //LEDS.setBrightness(timeBrightness());
    LEDS.setBrightness(ldrBrightness());
    //LEDS.setBrightness(brightness);
    //LEDS.setBrightness(MAX_BRIGHTNESS);
    //LEDS.setBrightness(sunsetBrightness());
    
    // if not connected, then show waiting animation
    if(timeStatus() == timeNotSet) {
        // show initialisation animation
        Serial.println("time not yet known");
        for(int i=0;i<NUM_LEDS;++i){ //blank rest
            leds[i] = CRGB::Black;
        }

        float phase = ((float)(millis()%2000)) / 1000;
        if(phase > 1) phase = 2.0f-phase;
        for(int i=0;i<4;++i){  // the scanner moves from 0 to(inc) 5, but only 1..4 are actually shown on the four leds
            float intensity = abs((float)(i-1)/3-phase);
            intensity = sqrt(intensity);
            leds[i] = CRGB(255-(255*intensity),0,0);
        }
        FastLED.show();
        return;
    }

    time_t t = now();

    // calculate target brightnesses:
    int current_hourword = hour();
    if(current_hourword>12) current_hourword = current_hourword - 12; // 12 hour clock, where 12 stays 12 and 13 becomes one
    if(current_hourword==0) current_hourword = 12;            // 0 is also called 12
    
    int next_hourword = hour()+1;
    if(next_hourword>12) next_hourword = next_hourword - 12;      // 12 hour clock, where 12 stays 12 and 13 becomes one
    if(next_hourword==0) next_hourword = 12;              // 0 is also called 12

    for(int i=0;i<NUM_LEDS;++i) {
        targetlevels[0][i] = 0;
        targetlevels[1][i] = 0;
        targetlevels[2][i] = 0;
    }

    #ifdef DUTCH
    show_word(HET);
    show_word(IS);

    switch((minute()%60)/5) {
        case 0:
            if(current_hourword==5) current_hourword = VIJF;  // use the other five to display five o'clock because it looks better
            show_word(current_hourword);
            show_word(UUR);
            break;
        case 1:
            show_word(VIJF);
            show_word(OVER);
            show_word(current_hourword);
            break;
        case 2:
            show_word(TIEN);
            show_word(OVER);
            show_word(current_hourword);
            break;
        case 3:
            show_word(KWART);
            show_word(OVER);
            show_word(current_hourword);
            break;
        case 4:
            show_word(TIEN);
            show_word(VOOR);
            show_word(HALF);
            show_word(next_hourword);
            break;
        case 5:
            show_word(VIJF);
            show_word(VOOR);
            show_word(HALF);
            show_word(next_hourword);
            break;
        case 6:
            show_word(HALF);
            show_word(next_hourword);
            break;
        case 7:
            show_word(VIJF);
            show_word(OVER);
            show_word(HALF);
            show_word(next_hourword);
            break;
        case 8:
            show_word(TIEN);
            show_word(OVER);
            show_word(HALF);
            show_word(next_hourword);
            break;
        case 9:
            show_word(KWART);
            show_word(VOOR);
            show_word(next_hourword);
            break;
        case 10:
            show_word(TIEN);
            show_word(VOOR);
            show_word(next_hourword);
            break;
        case 11:
            show_word(VIJF);
            show_word(VOOR);
            show_word(next_hourword);
            break;
    }
    #endif

    #ifdef ENGLISH
    show_word(IT);
    show_word(IS);
    switch((minute()%60)/5) {
    case 0:
        show_word(current_hourword);
        show_word(OCLOCK);
        break;
    case 1:
        show_word(FIVE);
        show_word(PAST);
        show_word(current_hourword);
        break;
    case 2:
        show_word(TEN);
        show_word(PAST);
        show_word(current_hourword);
        break;
    case 3:
        show_word(A);
        show_word(QUARTER);
        show_word(PAST);
        show_word(current_hourword);
        break;        
    case 4:
        show_word(TWENTY);
        show_word(PAST);
        show_word(current_hourword);
        break;        
    case 5:
        show_word(TWENTYFIVE);
        show_word(PAST);
        show_word(current_hourword);
        break;        
    case 6:
        show_word(HALF);
        show_word(PAST);
        show_word(current_hourword);
        break;        
    case 7:
        show_word(TWENTYFIVE);
        show_word(TO);
        show_word(next_hourword);
        break;        
    case 8:
        show_word(TWENTY);
        show_word(TO);
        show_word(next_hourword);
        break;        
    case 9:
        show_word(A);
        show_word(QUARTER);
        show_word(TO);
        show_word(next_hourword);
        break;        
    case 10:
        show_word(TEN);
        show_word(TO);
        show_word(next_hourword);
        break;        
    case 11:
        show_word(FIVE);
        show_word(TO);
        show_word(next_hourword);
        break;        
    }
 
    #endif

    // the minute leds at the bottom:
    for(int i=4-(minute()%5);i<4;++i) {
        targetlevels[0][i] = 0;
        targetlevels[1][i] = 255;
        targetlevels[2][i] = 255;
    }

    int speed = 4;


    // for brightness debugging:
    //for (int i=0; i<NUM_LEDS;i++) {
    //    targetlevels[i] = i <= brightness ? MAX_BRIGHTNESS : 0;
    //}

    // move current brightness towards target brightness:
    for(int i=0;i<NUM_LEDS;++i) {
        for(uint8_t c=0;c<3;c++) {
            if(currentlevels[c][i] < targetlevels[c][i]) {
                currentlevels[c][i] = constrain(currentlevels[c][i]+speed, 0, 255);
            }
            if(currentlevels[c][i] > targetlevels[c][i]) {
                currentlevels[c][i] = constrain(currentlevels[c][i]-speed, 0, 255);
            }

            // output the value to led: according to the function x^2/255 to compensate for the perceived brightness of leds which is not linear
            leds[i] = CRGB(
                //currentlevels[0][i]*currentlevels[0][i]/256,
                //currentlevels[1][i]*currentlevels[1][i]/256,
                //currentlevels[2][i]*currentlevels[2][i]/256);
                gammaR[currentlevels[0][i]],
                gammaG[currentlevels[1][i]],
                gammaB[currentlevels[2][i]]
            );
        }
    }
    /*
    Serial.print("target: ");
    Serial.print(targetlevels[0][84]);
    Serial.print(", ");
    Serial.print(targetlevels[1][84]);
    Serial.print(", ");
    Serial.println(targetlevels[2][84]);

    Serial.print("current: ");
    Serial.print(currentlevels[0][84]);
    Serial.print(", ");
    Serial.print(currentlevels[1][84]);
    Serial.print(", ");
    Serial.println(currentlevels[2][84]);
    */
    // Update LEDs
    FastLED.show();
}

/*-------- NTP code ----------*/

time_t getNtpTime()
{
    IPAddress ntpServerIP; // NTP server's ip address

    while (Udp.parsePacket() > 0) ; // discard any previously received packets
    Serial.println("Transmit NTP Request");
    // get a random server from the pool
    WiFi.hostByName(ntpServerName, ntpServerIP);
    Serial.print(ntpServerName);
    Serial.print(": ");
    Serial.println(ntpServerIP);
    sendNTPpacket(ntpServerIP);
    uint32_t beginWait = millis();
    while (millis() - beginWait < 1500) {
        int size = Udp.parsePacket();
        if (size >= NTP_PACKET_SIZE) {
            Serial.println("Receive NTP Response");
            Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
            unsigned long secsSince1900;
            // convert four bytes starting at location 40 to a long integer
            secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
            secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
            secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
            secsSince1900 |= (unsigned long)packetBuffer[43];
            // New time in seconds since Jan 1, 1970
            unsigned long newTime = secsSince1900 - 2208988800UL +
                timeZone * SECS_PER_HOUR;

            // Auto DST
            if (autoDST) {
                if (isDSTSwitchDay(day(newTime), month(newTime), year(newTime))) {
                    if (month(newTime) == 3 && hour(newTime) >= 2) {
                        newTime += SECS_PER_HOUR;
                    } else if (month(newTime) == 10 && hour(newTime) < 2) {
                        newTime += SECS_PER_HOUR;
                    }
                } else if (isDST(day(newTime), month(newTime), year(newTime))) {
                    newTime += SECS_PER_HOUR;
                }
            }

            setSyncInterval(SYNC_INTERVAL);
            return newTime;
        }
    }
    Serial.println("No NTP Response :-(");
    // Retry soon
    setSyncInterval(10);
    return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address)
{
    // set all bytes in the buffer to 0
    memset(packetBuffer, 0, NTP_PACKET_SIZE);
    // Initialize values needed to form NTP request
    // (see URL above for details on the packets)
    packetBuffer[0] = 0b11100011;   // LI, Version, Mode
    packetBuffer[1] = 0;     // Stratum, or type of clock
    packetBuffer[2] = 6;     // Polling Interval
    packetBuffer[3] = 0xEC;  // Peer Clock Precision
    // 8 bytes of zero for Root Delay & Root Dispersion
    packetBuffer[12] = 49;
    packetBuffer[13] = 0x4E;
    packetBuffer[14] = 49;
    packetBuffer[15] = 52;
    // all NTP fields have been given values, now
    // you can send a packet requesting a timestamp:
    Udp.beginPacket(address, 123); //NTP requests are to port 123
    Udp.write(packetBuffer, NTP_PACKET_SIZE);
    Udp.endPacket();
}

// Check if Daylight saving time (DST) applies
// Northern Hemisphere - +1 hour between March and October
bool isDST(int d, int m, int y){
    bool dst = false;
    dst = (m > 3 && m < 10); // October-March

    if (m == 3){
        // Last sunday of March
        dst = (d >= ((31 - (5 * y /4 + 4) % 7)));
    }else if (m == 10){
        // Last sunday of October
        dst = (d < ((31 - (5 * y /4 + 1) % 7)));
    }

    return dst;
}

bool isDSTSwitchDay(int d, int m, int y){
    bool dst = false;
    if (m == 3){
        // Last sunday of March
        dst = (d == ((31 - (5 * y /4 + 4) % 7)));
    }else if (m == 10){
        // Last sunday of October
        dst = (d == ((31 - (5 * y /4 + 1) % 7)));
    }
    return dst;
}

/*
v2:
IT IS HALF
QUARTERTEN
TWENTYFIVE
PASTO FOUR
TWONELEVEN
SEVENTHREE
TWELVEIGHT
FIVETENINE
SIX OCLOCK

v1:
IT IS TEN 
HALFIFTEEN
TWENTYFIVE
PASTO FOUR
TWONELEVEN
THREESEVEN
TWELVEIGHT
TENINEFIVE
SIX OCLOCK


*/

